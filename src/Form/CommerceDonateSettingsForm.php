<?php

namespace Drupal\commerce_donate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures commerce_donate settings.
 */
class CommerceDonateSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'commerce_donate.settings';

  /**
   * Single donation amounts:
   *
   * @var array
   */
  const SINGLE_DONATION_AMOUNTS = [50, 100, 250];

  /**
   * Monthly donation amounts:
   *
   * @var array
   */
  const MONTHLY_DONATION_AMOUNTS = [10, 21, 50];

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $commerce_donate_config = $this->config('commerce_donate.settings');
    $form['commerce_donate_settings'] = [
      '#type' => 'container',
      '#title' => $this->t('Commerce donate settings'),
    ];

    // Single donation.
    $form['commerce_donate_settings']['single_donation'] = [
      '#type' => 'details',
      '#title' => $this->t('Single donation settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    // Single donation - use predifined donation amounts.
    $form['commerce_donate_settings']['single_donation']['use_predefined_donation_amounts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use predefined amounts?'),
      '#default_value' => $commerce_donate_config->get('single_donation.use_predefined_donation_amounts'),
      '#description' => $this->t('Use the predefined single donation amounts @single_donation_amounts.', [
        '@single_donation_amounts' => implode(', ', self::SINGLE_DONATION_AMOUNTS)
      ]),
    ];

    // Single donation - donation amounts.
    $form['commerce_donate_settings']['donation_amounts'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $single_donation_donation_amounts = $commerce_donate_config->get('single_donation.donation_amounts');
    foreach ($single_donation_donation_amounts as $key => $donation_amount) {
      $form['commerce_donate_settings']['single_donation']['donation_amounts'][$key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Donation amount  @amount_number', ['@amount_number' => $key + 1]),
        '#default_value' => $donation_amount,
        '#states' => [
          'invisible' => [
            ':input[name="single_donation[use_predefined_donation_amounts]"]' => ['checked' => TRUE],
          ],
        ],
        '#required' => TRUE,
      ];
    }

    // Single donation - donation description.
    $form['commerce_donate_settings']['single_donation']['donation_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Donation description'),
      '#default_value' => $commerce_donate_config->get('single_donation.donation_description'),
      '#description' => t('Additional instructions about single donations, for example helpful tax or privacy information. Displays under amount selection.'),
    ];

    // Monthly donation.
    $form['commerce_donate_settings']['monthly_donation'] = [
      '#type' => 'details',
      '#title' => $this->t('Monthly donation settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Monthly donation - use predifined donation amounts.
    $form['commerce_donate_settings']['monthly_donation']['use_predefined_donation_amounts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use predefined amounts?'),
      '#default_value' => $commerce_donate_config->get('monthly_donation.use_predefined_donation_amounts'),
      '#description' => $this->t('Use the predefined monthly donation amounts @monthly_donation_amounts.', [
        '@monthly_donation_amounts' => implode(', ', self::MONTHLY_DONATION_AMOUNTS)
      ]),
    ];

    // Monthly donation - donation amounts.
    $form['commerce_donate_settings']['donation_amounts'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $monthly_donation_donation_amounts = $commerce_donate_config->get('monthly_donation.donation_amounts');
    foreach ($monthly_donation_donation_amounts as $key => $donation_amount) {
      $form['commerce_donate_settings']['monthly_donation']['donation_amounts'][$key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Donation amount  @amount_number', ['@amount_number' => $key + 1]),
        '#default_value' => $donation_amount,
        '#states' => [
          'invisible' => [
            ':input[name="monthly_donation[use_predefined_donation_amounts]"]' => ['checked' => TRUE],
          ],
        ],
        '#required' => TRUE,
      ];
    }

    // Monthly donation - donation description.
    $form['commerce_donate_settings']['monthly_donation']['donation_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Donation description'),
      '#default_value' => $commerce_donate_config->get('monthly_donation.donation_description'),
      '#description' => t('Additional instructions about monthly donations, for example helpful tax or privacy information. Displays under amount selection.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getEditableConfigNames() {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'commerce_donate_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Reset donation amount values if it is checked.
    foreach ($values as $key => $donation_type) {
      if (strpos($key, '_donation') !== FALSE) {
        if ($donation_type['use_predefined_donation_amounts']) {
          $values[$key]['donation_amounts'] = $key == 'single_donation' ? self::SINGLE_DONATION_AMOUNTS : self::MONTHLY_DONATION_AMOUNTS;
        }
      }
    }
    $this->config('commerce_donate.settings')
      ->set('single_donation', $values['single_donation'])
      ->set('monthly_donation', $values['monthly_donation'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    foreach ($values as $key => $donation_type) {
      if (strpos($key, '_donation') !== FALSE) {
        foreach ($donation_type['donation_amounts'] as $amount_key => $donation_amount) {
          if (!filter_var($donation_amount, FILTER_VALIDATE_FLOAT)) {
            $form_state->setErrorByName($key . '][donation_amounts][' . $amount_key, $this->t('Donation amount must be a float.'));
          }
        }
      }
    }
  }

}
